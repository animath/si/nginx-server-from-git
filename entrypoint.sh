#!/bin/sh

cd /var/www/html
[ -d .git ] || git clone ${NGINX_SERVER_GIT_URL} .

git pull
nginx&
python /hook/docker-hook -c sh /hook/update.sh
