FROM python:3-alpine

RUN apk --update add git nginx && \
    rm /var/cache/apk/*

RUN pip install requests --no-cache-dir

RUN mkdir /hook
RUN mkdir -p /var/www/html

# Configure nginx
RUN mkdir /run/nginx
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx.conf /etc/nginx/conf.d/mkdocs-server.conf
RUN rm /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /hook
COPY ./docker-hook /hook
COPY ./update.sh /hook

WORKDIR /var/www/html

ENTRYPOINT ["/hook/entrypoint.sh"]

EXPOSE 80
